package com.hadaria.kk;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView menu;

    private RecyclerView rv_restaurant;
    private FirebaseDatabase database;
    private DatabaseReference reference;

    private ProgressBar progressBar;

    private MyAdapter adapter;
    private List<dataRestaurant> listData;

    private SessionManager session;
    private String TAG ="HomeActivity";

    private Toolbar toolbar;

    private TextView tvDaftar, tvLogin, tvLogout;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = findViewById(R.id.my_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.chair);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        menu = findViewById(R.id.my_btn_menu);

        drawerLayout = (DrawerLayout) findViewById(R.id.my_drawer);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.my_navi);
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);

        tvDaftar = view.findViewById(R.id.tv_daftar);
        tvLogin = view.findViewById(R.id.tv_login);
        tvLogout = view.findViewById(R.id.tv_logout);
        tvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("ahahahhahhaha");
                startActivity(new Intent(HomeActivity.this, RegisterActivity.class));
            }
        });
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.logoutUser();
                tvLogin.setVisibility(View.VISIBLE);
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        session = new SessionManager(getApplicationContext());


        Log.d(TAG, "User Login Status" + session.isLoggedIn());

        if (session.isLoggedIn()) {
            tvLogin.setVisibility(View.GONE);
            tvDaftar.setVisibility(View.GONE);
            HashMap<String, String> user = session.getUserDetails();
            String username = user.get(SessionManager.KEY_USER);

            // email

            String password = user.get(SessionManager.KEY_PASS);

//            ID_USER = id;

        } else {
            tvLogout.setVisibility(View.GONE);
            ;
        }

        session.checkLogin();


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        progressBar = findViewById(R.id.my_progress);
        progressBar.setVisibility(View.VISIBLE);

        rv_restaurant = findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager LM = new LinearLayoutManager(getApplicationContext());
        rv_restaurant.setLayoutManager(LM);
        rv_restaurant.setItemAnimator(new DefaultItemAnimator());
        rv_restaurant.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        rv_restaurant.setHasFixedSize(true);

        listData = new ArrayList<>();
        adapter = new MyAdapter(listData, this, rv_restaurant);

        database = FirebaseDatabase.getInstance();
        getDataFireBase();


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return true;
    }

    void getDataFireBase(){
        reference = database.getReference("rastaurant");

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                progressBar.setVisibility(View.GONE);
                dataRestaurant data = dataSnapshot.getValue(dataRestaurant.class);
                String haha = String.valueOf(dataSnapshot.child("kursi").getValue());
                System.out.println(haha);
                listData.add(data);
                rv_restaurant.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                dataRestaurant data = dataSnapshot.getValue(dataRestaurant.class);

                int index = getItemIndex(data);
                listData.set(index, data);
                adapter.notifyItemChanged(index);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    int getItemIndex(dataRestaurant data){
        int index = 0;
        for(int i = 0; i<listData.size(); i++){
            if(listData.get(i).getNama_restaurant().equals(data.getNama_restaurant())){
                index = i;
                break;
            }
        }
        return index;
    }

    public void mendaftar(View view) {
        System.out.println("klik klik");
    }
}
