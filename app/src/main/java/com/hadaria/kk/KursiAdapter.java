package com.hadaria.kk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by userundie on 12/24/2017.
 */

public class KursiAdapter extends RecyclerView.Adapter<KursiAdapter.ViewHolder>{

    Context ctx;
    List<Long> list;
    View view;
    RecyclerView recyclerView;

    public KursiAdapter(List<Long> list, Context ctx, RecyclerView recyclerView){
        this.list=list;
        this.ctx = ctx;
        this.recyclerView = recyclerView;
    }

    @Override
    public KursiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(ctx).inflate(R.layout.item_kursi, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int position = recyclerView.getChildLayoutPosition(view);
//                Kursi data = list.get(position);
//                Intent intent = new Intent(ctx, KursiActivity.class);
//                intent.putExtra("id", data.getId());
//                System.out.println(data.getId());
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                ctx.startActivity(intent);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KursiAdapter.ViewHolder holder, final int position) {

       if(list.get(position)==1){
           holder.image.setImageResource(R.drawable.on);
       }else if(list.get(position)==0){
           holder.image.setImageResource(R.drawable.of);
       }
           holder.no.setText(String.valueOf(position+1));



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView image;
        LinearLayout layar;
        TextView no;
        public ViewHolder(View view) {
            super(view);

            no = view.findViewById(R.id.nomor);
            image = view.findViewById(R.id.my_image_view);
            layar = view.findViewById(R.id.layar);


        }
    }
}