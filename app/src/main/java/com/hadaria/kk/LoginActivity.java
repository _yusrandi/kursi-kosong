package com.hadaria.kk;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private ImageView back;
    private EditText tvNama, tvUser, tvPass;
    private AlertDialog.Builder builder;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        back = findViewById(R.id.my_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvUser = findViewById(R.id.et_user);
        tvPass = findViewById(R.id.et_pass);

        builder = new AlertDialog.Builder(LoginActivity.this);
        session = new SessionManager(getApplicationContext());
    }

    public void login(View view) {

        String user = tvUser.getText().toString();
        String pass = tvPass.getText().toString();

        if (user.equals("") || pass.equals("")) {

            builder.setTitle("something went wrong ");
            builder.setMessage("please fill all the fields");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        else {

            login();

        }
    }

    private void login() {
        final String user = tvUser.getText().toString();
        final String pass = tvPass.getText().toString();

        System.out.println("username "+user + "  password "+pass);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("user");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String username = (String) postSnapshot.child("username").getValue();
                    String password = (String) postSnapshot.child("password").getValue();

                    System.out.println("Username "+postSnapshot.child("username").getValue());
                    System.out.println("Password "+postSnapshot.child("password").getValue());
                    if(user.equals(username) && pass.equals(password)){
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        session.createLoginSession(user, pass);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
