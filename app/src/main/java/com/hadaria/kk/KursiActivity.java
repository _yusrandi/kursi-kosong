package com.hadaria.kk;

import android.content.res.Resources;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KursiActivity extends AppCompatActivity {

    private TextView myId;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private KursiAdapter adapter;
    private List<Long> listData;
    private RecyclerView rv_kursi;
    private ImageView back;
    private MediaPlayer player;

    public String Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kursi);

        player = MediaPlayer.create(KursiActivity.this, R.raw.definite);

        Id = String.valueOf(getIntent().getSerializableExtra("id"));

        back = findViewById(R.id.my_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        rv_kursi = findViewById(R.id.my_recycler_view_kursi);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 4);
        rv_kursi.setLayoutManager(mLayoutManager);
        rv_kursi.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(2), true));
        rv_kursi.setItemAnimator(new DefaultItemAnimator());
        rv_kursi.setAdapter(adapter);

        listData = new ArrayList<>();
        adapter = new KursiAdapter(listData, this, rv_kursi);
        rv_kursi.setAdapter(adapter);

        database = FirebaseDatabase.getInstance();
        getDataFireBase();



    }
    void getDataFireBase(){
        reference = database.getReference("kursi");

        reference.addChildEventListener(new ChildEventListener() {


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                player.start();

                String id = (String) dataSnapshot.child("id").getValue();
                System.out.println("is nya "+id);

                if(Id.equals(id)){
                    DataSnapshot contactSnapshot = dataSnapshot.child("nomor");
                    Iterable<DataSnapshot> contactChildren = contactSnapshot.getChildren();
                    for (DataSnapshot contact : contactChildren) {
                        Long c = contact.getValue(Long.class);
                        System.out.println(c);
                        listData.add(c);

                    }
                }




                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                player.start();

                listData.clear();

                String id = (String) dataSnapshot.child("id").getValue();
                System.out.println("is nya "+id);

                if(Id.equals(id)){
                    DataSnapshot contactSnapshot = dataSnapshot.child("nomor");
                    Iterable<DataSnapshot> contactChildren = contactSnapshot.getChildren();
                    for (DataSnapshot contact : contactChildren) {
                        Long c = contact.getValue(Long.class);
                        System.out.println(c);
                        listData.add(c);

                    }
                }




                adapter.notifyDataSetChanged();





            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    int getItemIndex(List<Long> data){
        int index = 0;
        for(int i = 0; i<listData.size(); i++){
            if(listData.get(i) == data.get(i)){
                index = i;
                break;
            }
        }
        return index;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
