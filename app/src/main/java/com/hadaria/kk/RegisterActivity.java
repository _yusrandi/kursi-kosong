package com.hadaria.kk;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private ImageView back;
    private EditText tvNama, tvUser, tvPass;
    private AlertDialog.Builder builder;
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        back = findViewById(R.id.my_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvNama = findViewById(R.id.et_nama);
        tvUser = findViewById(R.id.et_user);
        tvPass = findViewById(R.id.et_pass);

        builder = new AlertDialog.Builder(RegisterActivity.this);
        session = new SessionManager(getApplicationContext());


    }

    public void getDataFireBase() {
        String nama = tvNama.getText().toString();
        String user = tvUser.getText().toString();
        String pass = tvPass.getText().toString();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("user");

        user u = new user(nama, user, pass);
        ref.push().setValue(u);
        final String status = "OK";
        builder.setTitle("OK");
        builder.setMessage("Thankz for register with us, now u can login");



        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (status.equals("OK")) {
                    Intent x = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(x);
                    finish();
                } else if (status.equals("FAIL")) {

                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void mendaftar(View view) {

        String nama = tvNama.getText().toString();
        String user = tvUser.getText().toString();
        String pass = tvPass.getText().toString();

        if (nama.equals("") || user.equals("") || pass.equals("")) {

            builder.setTitle("something went wrong ");
            builder.setMessage("please fill all the fields");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        else {

            getDataFireBase();

        }

    }
}
