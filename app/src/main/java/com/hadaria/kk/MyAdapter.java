package com.hadaria.kk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by userundie on 12/24/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

    Context ctx;
    List<dataRestaurant> list;
    View view;
    RecyclerView recyclerView;

    public MyAdapter(List<dataRestaurant> list, Context ctx, RecyclerView recyclerView){
        this.list=list;
        this.ctx = ctx;
        this.recyclerView = recyclerView;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(ctx).inflate(R.layout.item_res, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = recyclerView.getChildLayoutPosition(view);
                dataRestaurant data = list.get(position);
                Intent intent = new Intent(ctx, KursiActivity.class);
                intent.putExtra("id", data.getId());
                System.out.println(data.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, final int position) {

        final dataRestaurant data = list.get(position);

        Picasso.with(ctx).load(data.getUrl_image()).into(holder.image);
        holder.nama.setText(data.getNama_restaurant());
        holder.alamat.setText(data.getAlamat());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nama, alamat;
        ImageView image;
        public ViewHolder(View view) {
            super(view);

            nama = view.findViewById(R.id.my_nama_restaurant);
            alamat = view.findViewById(R.id.my_alamat_restaurant);
            image = view.findViewById(R.id.my_image_view);

        }
    }
}