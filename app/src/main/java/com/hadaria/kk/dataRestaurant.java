package com.hadaria.kk;

import java.util.HashMap;
import java.util.List;

/**
 * Created by userundie on 12/23/2017.
 */

public class dataRestaurant {
    String nama_restaurant, url_image, alamat, id;


    public String getNama_restaurant() {
        return nama_restaurant;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setNama_restaurant(String nama_restaurant) {
        this.nama_restaurant = nama_restaurant;
    }

    public String getId() {
        return id;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }


}
